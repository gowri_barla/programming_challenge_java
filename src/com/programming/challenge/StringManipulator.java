package com.programming.challenge;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class StringManipulator 
{

	/*
	*   Function    : main
	*   Description : This function read in a .csv file located in src/atdd/challenge_tests.csv 
	*                 execute each of the words in the file and outputs the Words in Upper Case,
	*                 Whether words are �Unique� or not and �Weight of each word
	*                 Words should be completely alphabetic and in all upper-case letters.
	*
	*/
	public static void main(String args[]) {
		BufferedReader br = null;
		try {
			// Reading the csv file
			br = new BufferedReader(new FileReader("./src/atdd/challenge_tests.csv"));

			// Create List for holding Words objects
			List<String> words = new ArrayList<String>();
			String line = "";
			// Read to skip the header
			br.readLine();
			// Reading from the second line
			while ((line = br.readLine()) != null) {
				words.add(line);

			}
			//
			String[] w = new String[words.size()];
			sortStrings(words.toArray(w));
			br.close();

		} catch (Exception ee) {
			ee.printStackTrace();
		}

	}
	
	/*
	*  Function    : cleanString
	*  Description : This function removes all non-alphabetic characters from a string and 
	*                 converts all characters to upper case.
	*
	*     Input     :
	*     String    :  Raw String with alphabetic or non-alphabetic characters 
	*
	*     Output    :
	*     String    :  String with Upper Case alphabetic characters
	*
	*/
	public static String cleanString(String str) {
		// Replace all non-alphabetic characters with empty string and converting to
		// Upper Case
		str = str.replaceAll("[^a-zA-Z]", "").toUpperCase();
		return str;

	}
	
	/*
	*   Function    : hasUniqueChars
	*   Description : This function to check word only contains unique characters. 
	*                 Not considering the non-alphabetic characters and word should not be case-sensitive
	*
	*     Input     :
	*     String    :  Raw String with alphabetic or non-alphabetic characters 
	*
	*     Output    :
	*     Boolean   :  Returns True if unique characters in word
	*                  False  if no unique characters in word 
	*
	*/
	public static boolean hasUniqueChars(String word) {

		// Converting word to array of characters and loop over
		for (char ch : word.toCharArray()) {
			// Comparing whether the index of character is equal to last character in string
			if (word.indexOf(ch) == word.lastIndexOf(ch))
				continue;
			else
				return false; // if no unique characters in word
		}
		return true; // if unique characters in word
	}
	
 	/*
 	*   Function    : getWeight
 	*   Description : This function to check the weight of a string. The weight is defined as a
    *                 numeric value that is the average of each char ASCII value contained in the string
 	*
 	*     Input     :
 	*     String    :  Raw String with alphabetic or non-alphabetic characters 
 	*
 	*     Output    :
 	*     float     :  Returns weight of the string 
 	*
 	*/
	public static float getWeight(String word) {
		float weight = 0; // we have declare this as float because output will give as integer because
							// integer divided float gives integer
		word = cleanString(word);
		for (char ch : word.toCharArray()) {
			weight += (int) ch;
		}
		return weight / word.length();
	}
	
 	/*
 	*   Function:    sortStrings
 	*   Description: This function sort a large array of strings based on the value obtained
	*                by the weight function in ascending order.
 	*
 	*     Input          :
 	*     String []      :  Array of strings 
 	*
 	*     Output         :
 	*                    :  Sorted array of strings written directly to the file "challenge_sorted.csv"
	*                       location("./src/atdd")
 	*
 	*/
	public static void sortStrings(String[] words) {
		List<Sort> list = new ArrayList<Sort>();
		//Cleaning the words, Finding the word has unique characters or not 
		//and getting weight of word
		for (String word : words) {
			word = cleanString(word);
			String isUnique = hasUniqueChars(word) ? "TRUE" : "FALSE";
			list.add(new Sort(word, getWeight(word), isUnique));
		}
        //Sorting the words as per weight
		list.sort(new Comparator<Sort>() {
			@Override
			public int compare(Sort lhs, Sort rhs)
			{
			    return lhs.weight < rhs.weight ? -1 : lhs.weight > rhs.weight ? 1 : 0;
			}
		});
        //Writing the output to the csv file
		try {
			FileWriter fw = new FileWriter(new File("./src/atdd/challenge_sorted.csv"));
			//writing the Header into the file
			String writeline = "Words" + "," + "Unique" + "," + "Weight"+ "\n";
			fw.write(writeline);
			//writing processed data into the file 
			for (Sort s : list) {
				writeline = s.word + "," + s.isUnique + "," + Float.toString(s.weight) + "\n";
				fw.write(writeline);
			}

			fw.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

class Sort {
	String word;
	float weight;
	String isUnique;

	Sort(String word, float weight, String isUnique) {
		this.word = word;
		this.weight = weight;
		this.isUnique = isUnique;
	}
}
	
	
